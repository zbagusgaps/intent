package com.example.MAID;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Inside1 extends AppCompatActivity {

    public static String Extra_Username;

    Button btnMoreInfo;
    TextView text;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inside1);

        btnMoreInfo = findViewById(R.id.BtnInsideMoreInfo);
        text = findViewById(R.id.Tvtext);

        String nama = getIntent().getStringExtra("username");
        String email = getIntent().getStringExtra("email");
        text.setText("Hallo " + nama + ", \nyour email is : " + email);

        btnMoreInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://www.youtube.com/watch?v=dQw4w9WgXcQ&ab_channel=RickAstleyVEVO"));
                startActivity(intent);
            }
        });

    }
}
